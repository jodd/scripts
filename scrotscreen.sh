#!/usr/bin/bash

# scrot makes a screenshot
## --silent, -z
## --delay NUM, -d NUM	NUM=sekunder
## --exec APP, -e APP	-e feh åpner screenshot med feh
## --select, -s 		interaktiv
## %s is unix time

/usr/bin/scrot /home/Pictures/screenshots/scrshot_%s.png
pgrep -x dunst && notify-send "scrot" "Screenshot saved @ ~/Pictures/screenshots"

