#!/usr/bin/bash

# TODO
# term=
name="qutebrowser"
runf="/usr/bin/qutebrowser"

desktop_nr=$(bspc rule --list |\
	grep $name |\
	cut -d'=' -f3 |\
	cut -d' ' -f1)

node_id=$(xwininfo -root -tree |\
	grep "$name\")" |\
	tail -1 |\
	cut -d' ' -f6 |\
	tail -1)

if [ $node_id ]; then
	bspc desktop --focus $desktop_nr
else
	$runf
	bspc desktop --focus $desktop_nr
fi

