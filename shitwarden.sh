#!/usr/bin/bash

name="Bitwarden"
runf="/usr/bin/bitwarden"

show_bitwarden() {
    node_id=$(cat /tmp/node_id_bitwarden)
    bspc node $node_id --flag hidden
    bspc node $node_id --focus 
    exit 0
}

kill_bitwarden() {
    pidof bitwarden>/dev/null && killall bitwarden
    [ -f /tmp/node_id_bitwarden ] && rm /tmp/node_id_bitwarden
    return 0
}

start_bitwarden() {
    $runf&
    sleep 1
    xwininfo -root -tree | grep "$name\")" | tail -1 | cut -d' ' -f6  > /tmp/node_id_bitwarden
    exit 0
}

pidof bitwarden>/dev/null && show_bitwarden
kill_bitwarden
start_bitwarden

## snipped from original

# term=
name="Bitwarden"
runf="/usr/bin/bitwarden"

if (pidof bitwarden >/dev/null); then
    [ -f /tmp/node_id_bitwarden ] && node_id=$(cat /tmp/node_id_bitwarden)
    [ -f /tmp/node_id_bitwarden ] || xwininfo -root -tree | grep "$name\")" | tail -1 | cut -d' ' -f6 > /tmp/node_id_bitwarden
    node_id=$(cat /tmp/node_id_bitwarden)
    bspc node $node_id --flag hidden
    bspc node $node_id --focus >/dev/null
else
    [ -f /tmp/node_id_bitwarden ] && rm /tmp/node_id_bitwarden
    $runf&
    sleep 1
    xwininfo -root -tree | grep "$name\")" | tail -1 | cut -d' ' -f6 > /tmp/node_id_bitwarden
fi  

# Contains a lot of double tests, but I got tired
# One problem was before i added 'sleep 1' xwininfo didn't find any process

#pidof bitwarden > /dev/null && \
#    [ -f /tmp/bitwardenid ] && \
#    node_id=$(cat /tmp/bitwardenid) && \
#    bspc node $node_id --flag hidden ; \
#    bspc node $node_id --focus >/dev/null || \
#    [ -f /tmp/bitwardenid ] && rm /tmp/bitwardenid ; \
#    pidof bitwarden > /dev/null || $runf& ; sleep 1 && \
#    xwininfo -root -tree | grep "$name\")" | tail -1 | cut -d' ' -f6 > /tmp/bitwardenid 
#
#pidof bitwarden >/dev/null || rm /tmp/node_id_bitwarden

#Start_Bitwarden() {
#    pidowf bitwarden && killall bitwarden
#    [ -f /tmp/node_id_bitwarden ] && rm /tmp/node_id_bitwarden
#    $runf&
#    time 1
#    xwininfo -root -tree | grep "$name\")" | tail -1 | cut -d' ' -f6 > /tmp/node_id_bitwarden
#}


#pidof bitwarden >/dev/null || Start_Bitwarden
#
#pidof bitwarden >/dev/null && \
#    [ -f /tmp/node_id_bitwarden ] && \
#    node_id=$(cat /tmp/node_id_bitwarden) && \
#    bspc node $node_id --flag hidden && \
#    bspc node $node_id --focus >/dev/null



#[ -f /tmp/node_id_bitwarden ] && \
#    pidof bitwarden >/dev/null && \
#    node_id=$(cat /tmp/node_id_bitwarden) || \
#    [ -f /tmp/node_id_bitwarden ] && \
#    rm /tmp/node_id_bitwarden && \


#pidof bitwarden >/dev/null || \
#    runf& ; sleep 1 ; \
#    [ -f /tmp/node_id_bitwarden ] && \
#    rm /tmp/node_id_bitwarden ; \
#    xwininfo -root -tree | grep "$name\")" | tail -1 | cut -d' ' -f6 >/tmp/node_id_bitwarden


#    [ -f /tmp/node_id_bitwarden ] && \
#    rm /tmp/node_id_bitwarden && \
#    xwininfo -root -tree | grep "$name\")" | tail -1 | cut -d' ' -f6>/tmp/node_id_bitwarden

#    node_id
#
#[ -f /tmp/node_id_bitwarden ] 
#    
#    bspc node $node_id --flag hidden &&
#    bspc node $node_id --focus >/dev/null ; \
#    date +%T >> /tmp/nodlog
