#!/usr/bin/bash

show_bitwarden() {
    node_id=$(< /tmp/node_id_bitwarden)
    bspc node $node_id --flag hidden
    bspc node $node_id --focus 
    exit 0
}

kill_bitwarden() {
    pidof bitwarden>/dev/null && killall bitwarden
    [ -f /tmp/node_id_bitwarden ] && rm /tmp/node_id_bitwarden
    return 0
}

start_bitwarden() {
    /usr/bin/bitwarden&
    sleep 1
    xwininfo -root -tree | grep "Bitwarden\")" | tail -1 | cut -d' ' -f6  > /tmp/node_id_bitwarden
    exit 0
}

pidof bitwarden>/dev/null && show_bitwarden
kill_bitwarden
start_bitwarden
