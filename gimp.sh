#!/usr/bin/bash



running_darktable=$(pidof darktable)
running_gimp=$(pidof gimp)

[ -z running_darktable ] || $HOME/.local/bin/darktable
[ -z running_gimp ] || /usr/bin/gimp
bspc desktop --focus 5
