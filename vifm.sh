#!/usr/bin/bash
# starts program (runf) with name (name) in st terminal (term)

# term="/usr/local/bin/st"
term="/usr/bin/alacritty"
name="VIFM"
# runf="/home/jan/.config/vifm/scripts/vifmrun"
runf="/usr/bin/vifm"

# both st and alacritty accept -t
flag_name="-t $name"

# st needs -n, alacritty need --class
# flag_class="-n $name"
flag_class="--class $name"

xwininfo -root -tree | grep "(\"$name" 1>/dev/null || $term $flag_name $flag_class -e $runf&

bspc desktop -f $(bspc rule -l | grep -i $name | cut -d'=' -f3)


