#!/bin/bash

# script som vil lete etter kamera og koble det til, deretter laste ned
# alle bilder.

# gphoto2 
# --list-ports
# --auto-detect
# --list-files
# --get-all-files

# open folder:
# /home/Pictures/inn-fra-camera

cd /home/Pictures/inn-fra-camera && \
    /usr/bin/gphoto2 --auto-detect && \
    /usr/bin/gphoto2 --get-all-files 


## This script makes it happen, but there's something wrong.
## It does'n work in a vivible window
## Theres no progress
## But files get uploaded where they're supposed to to
