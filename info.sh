#!/usr/bin/bash

# Get some information and send it to dunst:

current_ip=$(curl ifconfig.me/ip)

notify-send 'MY IP' "$current_ip"

