#!/bin/bash
##
# rsync i en mer presentabel form, oversiktlig,
#
# Men det vil bli mange bolker, da det går mange linjer for hver

rsync\
	--dry-run\		# Test run
	--archive\		# -a archive mode
	--verbose\		# -v Some explaining text
	--update\		# -u update, skip newer date on receiver
	--append\		# append data on shorter files
	--recursive\	# -r goes in to directories
	--rsh=ssh\		# -e remote shell
	~/.config/*\	# 
	--exclude="autostart*"\
	--exclude="google-chrome"\
	--exclude="opera"\
	--exclude="VirtualBox"\
	--exclude="libreoffice"\
	--exclude="pulse"\
	--exclude="vlc"\
   	jan@mink:/home/jan/TRETERRAID/config

