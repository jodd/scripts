#!/bin/bash

## Killing some know programs that's usually there

kill $(ps -e | sed -n '/galculator/p' | awk -F " " '{print $1}') 2> /dev/null
kill $(ps -e | sed -n '/keepassx2/p' | awk -F " " '{print $1}') 2> /dev/null

poweroff
