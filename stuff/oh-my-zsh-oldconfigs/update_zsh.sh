#!/bin/bash

cd ~/.oh-my-zsh
git stash
tools/upgrade_oh_my_zsh
git stash pop
cd ~

# can't upgrade when theres changes to themes and .gitignore
# changes are stashed
# then theres a upgrade
# changes are reapplied
