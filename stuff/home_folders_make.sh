#!/usr/bin/bash

case $1 in
    r)
        [ -L "$HOME"/Desktop ] && rm "$HOME"/Desktop
        [ -L "$HOME"/Documents ] && rm "$HOME"/Documents
        [ -L "$HOME"/Downloads ] && rm "$HOME"/Downloads
        [ -L "$HOME"/Videos ] && rm "$HOME"/Videos
        [ -L "$HOME"/Music ] && rm "$HOME"/Music
        [ -L "$HOME"/Templates ] && rm "$HOME"/Templates
        [ -L "$HOME"/Pictures ] && rm "$HOME"/Pictures
        ;;
    m)
        [ -L "$HOME"/Desktop ] || ln -s "$DDRIVE"/Desktop "$HOME"/Desktop
        [ -L "$HOME"/Documents ] || ln -s "$DDRIVE"/Documents "$HOME"/Documents
        [ -L "$HOME"/Downloads ] || ln -s "$DDRIVE"/Downloads "$HOME"/Downloads
        [ -L "$HOME"/Videos ] || ln -s "$DDRIVE"/Videos "$HOME"/Videos
        [ -L "$HOME"/Music ] || ln -s "$DDRIVE"/Music "$HOME"/Music
        [ -L "$HOME"/Templates ] || ln -s "$DDRIVE"/Templates "$HOME"/Templates
        [ -L "$HOME"/Pictures ] || ln -s "$DDRIVE"/Pictures "$HOME"/Pictures
        ;;
    *)
        printf "\nEither\tr (remove)\nOr\t m (make)\nStandard folders...\n"
        ;;
esac
