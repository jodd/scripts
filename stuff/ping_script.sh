#!/bin/sh
NUM=1
while [ $NUM -lt 256 ];do
	ping -q -c 1 10.0.0.$NUM > /dev/null
	RESULT=$(echo $?)
	if [ $RESULT -eq 0 ];then
		printf 10.0.0.$NUM"\n"
	fi
	NUM=$(expr $NUM + 1)
done

# this is slow, but finds ip's
# the number 2 is very quick

