#!/bin/sh

# if there is no arg, then exit
[ -z "$1" ] && exit

if xwininfo -tree -root | grep "(\"$1\" ";
then
	echo "Window Detected"
else
	echo "No Window Detected"
	i3 "exec $term -n $1 $(echo "$@" | cut -d ' ' -f2) -e $1" && i3 "[instance=\"$1\"] scratchpad show; [instance=\"$1\"} move position center"
	sleep .25

fi
i3 "[instance=\"$1\"] scratchpad show; [instance=\"$1\"] move position center"

