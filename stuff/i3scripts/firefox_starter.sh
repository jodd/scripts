#!/bin/sh

# Checking if firefox is running, if not start it, then move to workplace
# workspace $1 must be passed on

if xwininfo -tree -root | grep "(\"firefox" ;
then
	echo "firefox is running"
	i3 "workspace $1" &&\
		i3 "[instance="firefox"] focus"
else
	i3 "workspace $1" &&\
		i3 "exec --no-startup-id /usr/bin/firefox" &&\
		i3 "[instance="firefox"] focus"
fi
#i3 "exec $1 --no-startup-id -name "firefox" -e /usr/bin/firefox" &&\
#trenger ikke $term...
	
