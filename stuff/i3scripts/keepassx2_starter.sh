#!/usr/bin/sh

# assumes keepassx2 exist
command -v keepassx2 || exit

if xwininfo -tree -root | grep "keepassx2\" \"Keepassx2";
then
	echo "There is a keepassx2 window"
	i3 "[instance=\"keepassx2\"] scratchpad show; \
		[instance=\"keepassx2\"] move position center"
else
	echo "Starting keepassx2"
	i3 "exec /usr/bin/keepassx2; \
		[instance=\"keepassx2\"] scratchpad show; \
		[instance=\"keepassx2\"] move position center"
fi
