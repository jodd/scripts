#!/bin/sh

# Checking if darktable is running, if not start it, then move to workplace

if [[ -d /home/mink/TRETERRAID/Pictures/jan ]]; then
	if xwininfo -tree -root | grep "(\"darktable" ;
	then
		echo "darktable is running"
		i3 "workspace $1" &&\
			i3 "[instance="darktable"] focus"
	else
		i3 "workspace $1" &&\
			i3 "exec --no-startup-id /usr/bin/darktable" &&\
			i3 "[instance="darktable"] focus"
	fi
else
	notify-send "mink is offline? >lmink"
	notify-send "Will not start darktable without logged in to mink."
	exit 1
fi
# mod+Shift+F5
# ~/.config/darktable
# ~/.cache/darktable/ -> /home/DATA/archi3/darktable/
# /usr/share/darktable
# /usr/lib/darktable
