#!/bin/bash

# Checking if qutebrowser is running.
# If not running, move to workspace $1
# Start qutebrowser

if xwininfo -tree -root | grep "(\"qutebrowser" ;
then
	echo "qutebrowser is running"
	i3 "workspace $1" &&\
		i3 "[instance="qutebrowser"] focus"
else
	i3 "workspace $1" &&\
		i3 "exec --no-startup-id /usr/bin/qutebrowser" &&\
		i3 "[instance="qutebrowser"] focus"
fi
#i3 "exec $2 --no-startup-id -name "qutebrowser" -e /usr/bin/qutebrowser" &&\
#trenger ikke $term...
