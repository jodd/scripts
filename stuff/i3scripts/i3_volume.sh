#!/bin/bash
## Audio control for jbm's i3

'''
bindsym $mod+Down exec amixer sset Master 5%-
bindsym $mod+Up exec amixer sset Master 5%+
bindsym $mod+Shift+Up exec amixer sset Master unmute
bindsym $mod+Shift+Down exec amixer sset Master mute
'''

## Denne kommando virker, jeg finner ikke noe i man pages om hvorfor
## pkill -RTMIN+10 i3blocks, den har med i3blocks og gjøre...
## denne oppdaterer signal 10
## Begge deler virker, men jeg skiftet til SIGRTMIN+10, lettere og huske
## avslutter og restarter i3blocks veldig raskt og ubemerkelig, slik at
## ny volume verdi korrigeres...

refresh="exec pkill -SIGRTMIN+10 i3blocks"

## Følgende argumenter: up,down,mute,unmute
case "$1" in
	"up") amixer sset Master 5%+ ; $refresh ;;
	"down")	amixer sset Master 5%- ; $refresh ;;
	"mute")	amixer sset Master mute ; $refresh ;;
	"unmute") amixer sset Master unmute; $refresh ;;
esac

