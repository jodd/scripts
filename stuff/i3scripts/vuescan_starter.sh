#!/bin/sh

# Checking if vuescan is running, if not start it, then move to workplace
# workspace $1 must be passed on

if xwininfo -tree -root | grep "(\"vuescan" ;
then
	echo "vuescan is running"
	i3 "workspace $1 " &&\
		i3 "[instance="vuescan"] focus" &&\
		i3 "layout tabbed"
else
	echo "/usr/local/bin/vuescan will be started"
	i3 "workspace $1 " &&\
		i3 "exec --no-startup-id /usr/local/bin/vuescan" &&\
		i3 "[instance="vuescan"] focus" 
fi
#i3 "exec $1 --no-startup-id -name "vuescan" -e /usr/bin/vuescan" &&\
#trenger ikke $term...
