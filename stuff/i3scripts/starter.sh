#!/bin/sh

## NEEDED for i3:
# command
# workspace number
# xwininfo -tree -root | grep -i command > see if command is running
# if command restarts new, it needs to be started spesial

#COMMANDTORUN=""
#WSNR=""
#INSTANCENAME=""

COMMANDTORUN="gimp"
WSNR="6"
INSTANCENAME="(^GNU Image|Gimp|gimp)"

if xwininfo -tree -root | grep -i $COMMANDTORUN ;
then
	echo "$COMMANDTORUN is running!"
	i3 "workspace $WSNR" &&\
		i3 "[instance="$INSTANCENAME"] focus"
else
	i3 "workspace $WSNR" &&\
		i3 "exec --no-startup-id $(command $COMMANDTORUN)" &&\
		i3 "[instance="$INSTANCENAME"] focus"
fi
