#!/bin/bash

# Checking if inkscape is running, if not start it, then move to workplace
# workspace $1 must be passed on

if xwininfo -tree -root | grep "(\"inkscape" ;
then
	echo "inkscape is running"
	i3 "workspace $1" &&\
		i3 "[instance="inkscape"] focus"
else
	i3 "workspace $1" &&\
		i3 "exec --no-startup-id /usr/bin/inkscape" &&\
		i3 "[instance="inkscape"] focus"
fi
#i3 "exec $1 --no-startup-id -name "inkscape" -e /usr/bin/inkscape" &&\
#trenger ikke $term...
