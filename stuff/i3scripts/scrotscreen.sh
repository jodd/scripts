#!/bin/bash

# scrot makes a screenshot
## --silent, -z
## --delay NUM, -d NUM	NUM=sekunder
## --exec APP, -e APP	-e feh åpner screenshot med feh
## --select, -s 		interaktiv
## %s is unix time

# if there is an argument for scrot else just scrot
[ $1 ] && scrot /home/DATA/Pictures/screenshots/scrshot_%s.png -e 'feh $f' || scrot /home/DATA/Pictures/screenshots/scrshot_%s.png


#notify-send "Screenshot saved @ ~/Pictures/screenshots"
