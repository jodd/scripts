#!/bin/sh

# Checking if chromium is running, if not start it, then move to workplace

if xwininfo -tree -root | grep "(\"chromium" ;
then
	echo "chromium is running"
	i3 "workspace $1" &&\
		i3 "[instance="chromium"] focus"
else
	i3 "workspace $1" &&\
		i3 "exec --no-startup-id /usr/bin/chromium" &&\
		i3 "[instance="chromium"] focus"
fi

	
		#i3 "exec $1 --no-startup-id -name "chromium" -e /usr/bin/chromium" &&\
