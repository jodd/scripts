#!/bin/sh

# script to start ranger, if ranger is running move to correct workplace given
# by i3, passed as $2
# needs term, as $term does not seems to work, passed 
# by i3 as $1


# Ranger is named ( -n for st, -name for urxvt) RANGER
# i exist then move ther, else open new.

if xwininfo -tree -root | grep "(\"RANGER" ;
then
	echo "Found ranger running..."
	i3 "workspace $2"
else
	echo "xwininfo did not find anything..."
	i3 "workspace $2; \
		exec --no-startup-id $1 -n \"RANGER\" -e /usr/bin/ranger"
fi


