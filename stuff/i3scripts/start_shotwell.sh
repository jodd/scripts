#!/bin/bash

# Shotwell in my setup, needs contact with mink, a server
#
# Pictures located in mink/TRETERRAID/Pictures/jan

if [[ -d /home/mink/TRETERRAID/Pictures/jan ]]; then
	if xwininfo -tree -root | grep "(\"shotwell" ;
	then
		i3 "workspace $2"
	else
			i3 "exec --no-startup-id /usr/bin/shotwell"
	fi
	i3 "workspace $2"
else
	echo "No Mink Today!!!"
	echo "I Shotwell The Sherif"
	exit 1;
fi

##
#	~/.local/share/shotwell/data/photo.db
#	~/.cache/shotwell -> /home/DATA/archi3/shotwell
#	/usr/lib/shotwell
#	/usr/bin/shotwell
#	config stored in .local/share... -> /home/DATA/archi3/shotwell

# TODO: change to permanent place, mink?

