#!/bin/bash

## bind to mod+y & mod+Shift+y
## Default is screen saver function is on, y turn it of
## The -dpms option disables DPMS (Energy Star) features.

## The +dpms option enables DPMS (Energy Star) features.

#Shift+y turn it on again

if [ $2="off" ]
then
	xset s off -dpms
else
	xset s on +dpms
fi


