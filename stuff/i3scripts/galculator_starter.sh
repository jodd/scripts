#!/usr/bin/sh

# assumes galculator exists
command -v galculator || exit

if xwininfo -tree -root | grep "galculator\" \"Galculator";
then
	echo "There's a galculator window open..."
	i3 "[instance=\"galculator\"] scratchpad show; \
		[instance=\"galculator\"] move position center "
else
	echo "There's no galculator, here we start it:"
	i3 "exec /usr/bin/galculator; \
		[instance=\"galculator\"] scratchpad show; \
		[instance=\"galculator\"] move position center "
fi

