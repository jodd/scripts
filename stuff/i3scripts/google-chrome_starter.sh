#!/bin/sh

# Checking if google-chrome is running, if not start it, then move to workplace

if xwininfo -tree -root | grep "(\"google-chrome" ;
then
	echo "google-chrome is running"
	i3 "workspace $1" &&\
		i3 "[instance="google-chrome"] focus"
else
	i3 "workspace $1" &&\
		i3 "exec --no-startup-id /usr/bin/google-chrome-stable" &&\
		i3 "[instance="google-chrome"] focus"
fi

	
