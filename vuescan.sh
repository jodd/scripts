#!/usr/bin/bash

# term=
name="vuescan"
runf="/usr/bin/vuescan"

xwininfo -root -tree | grep $name 1>/dev/null &&\
bspc desktop -f $(bspc rule -l | grep -i $name | cut -d'=' -f3) &&\
exit 0

$runf& 
bspc desktop -f $(bspc rule -l | grep -i $name | cut -d'=' -f3)

