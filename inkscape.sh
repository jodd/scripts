#/usr/bin/bash

running_inkscape=$(pidof inkscape)

bspc desktop -f 4
[ -z $running_inkscape ] && /usr/bin/inkscape &
